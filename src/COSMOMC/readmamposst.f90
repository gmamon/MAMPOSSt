Module ReadMamposst

  use IniFile
  use settings

  implicit none
  ! integer, parameter :: MAXCOMP = 4
  ! character (LEN=16) :: tracermodel(MAXCOMP), anismodel(MAXCOMP)
  ! real*8 :: rfid_tracer(MAXCOMP)

contains

 subroutine ReadStringMamposst(strg,ncomp,n)
   integer :: n, ncomp
   character(LEN=*) :: strg
   character(LEN=16) :: var

   print *,' entering ReadStringMamposst: ncomp = ', ncomp
   if (Ini_HasKey(strg)) then
      var = Ini_Read_String(strg)
      if (index(strg,'tracermodel') > 0) then
         ncomp = ncomp + 1
         if (n == 0) n = 1         
         tracermodel(n) = var
         n = n+1
      else if (index(strg,'anismodel') > 0) then
         if (n == 0) n = 1
         anismodel(n) = var
         n = n+1
      else   
         print *,' ReadStringMamposst cannot recognize string = ', &
              trim(strg)
      endif
      print *,' ReadStringMamposst: ncomp = ', ncomp
   endif

 end subroutine ReadStringMamposst

 subroutine ReadDoubleMamposst(strg,n)
   integer :: n
   character(LEN=*) :: strg
   real*8 :: var

   if (Ini_HasKey(strg)) then
      var = Ini_Read_Double(strg)
      if (index(strg,'rfid_tracer') > 0) then
         if (n == 0) n = 1         
         rfidtracer(n) = var
         n = n+1
      else   
         print *,' ReadDoubleMamposst cannot recognize string = ', &
              trim(strg)
      endif
   endif
 end subroutine ReadDoubleMamposst

 subroutine stringtowords(str,n,word)
 implicit none
 character (LEN=*), intent(IN) :: str
 integer :: n
 character(LEN=16) word(20)
 ! from http://rosettacode.org/wiki/Tokenize_a_string 
 INTEGER :: pos1 = 1, pos2, i
 n = 0
 DO while (.true.)
    ! first blank position in substring
    pos2 = INDEX(str(pos1:), ' ')
    IF (pos2 == 0) THEN
       n = n + 1
       word(n) = str(pos1:)
       return
    END IF
    n = n + 1
    if (n .gt. 20) then
       print *,' reached n = 20!'
       stop
    endif
    word(n) = str(pos1:pos1+pos2-2)
    pos1 = pos1 + pos2
 END DO
 end subroutine stringtowords

end module ReadMamposst

