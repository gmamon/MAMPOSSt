subroutine tautsp ( xknot, yknot, nknot, gamma, wk, break, coef, l, k, ier )

!*****************************************************************************80
!
!! TAUTSP constructs a cubic spline interpolant to given data.
!
!  Discussion:
!
!    If 0 < GAMMA, additional knots are introduced where needed to
!    make the interpolant more flexible locally.  This avoids extraneous
!    inflection points typical of cubic spline interpolation at knots to
!    rapidly changing data.
!
!  Method:  
!
!    On the I-th interval, (XKNOT(I), XKNOT(I+1)), the interpolant is of the
!    form:
!      (*)  F(U(X)) = A + B * U + C * H(U,Z) + D * H(1-U,1-Z),
!    with  
!      U = U(X) = ( X - XKNOT(I) ) / DXKNOT(I). 
!
!    Here,
!      Z(I) = ADDG(I+1) / ( ADDG(I) + ADDG(I+1) )
!    but if the denominator vanishes, we set Z(I) = 0.5
!
!    Also, we have
!      ADDG(J) = abs ( DDG(J) ), 
!      DDG(J) = DG(J+1) - DG(J),
!      DG(J) = DIVDIF(J) = ( YKNOT(J+1) - YKNOT(J) ) / DXKNOT(J)
!    and
!      H(U,Z) = ALPHA * U^3 
!           + ( 1 - ALPHA ) * ( max ( ( ( U - ZETA ) / ( 1 - ZETA ) ), 0 )^3
!    with
!      ALPHA(Z) = ( 1 - GAMMA / 3 ) / ZETA
!      ZETA(Z) = 1 - GAMMA * min ( ( 1 - Z ), 1/3 )
!
!    Thus, for 1/3 <= Z <= 2/3, F is just a cubic polynomial on
!    the interval I.  Otherwise, it has one additional knot, at
!      XKNOT(I) + ZETA * DXKNOT(I).
!
!    As Z approaches 1, H(.,Z) has an increasingly sharp bend near 1,
!    thus allowing F to turn rapidly near the additional knot.
!
!    In terms of F(J) = YKNOT(J) and FSECND(J) = second derivative of F 
!    at XKNOT(J), the coefficients for (*) are given as:
!      A = F(I) - D
!      B = ( F(I+1) - F(I) ) - ( C - D )
!      C = FSECND(I+1) * DXKNOT(I)^2 / HSECND(1,Z)
!      D = FSECND(I) * DXKNOT(I)^2 / HSECND(1,1-Z)
!
!    Hence these can be computed once FSECND(1:NKNOT) is fixed.
!
!    F is automatically continuous and has a continuous second derivative
!    except when Z=0 or 1 for some I.  We determine FSECND from
!    the requirement that the first derivative of F be continuous.
!
!    In addition, we require that the third derivative be continuous
!    across XKNOT(2) and across XKNOT(NKNOT-1).  This leads to a strictly
!    diagonally dominant tridiagonal linear system for the FSECND(I)
!    which we solve by Gauss elimination without pivoting.
!
!  Modified:
!
!    14 February 2007
!
!  Author:
!
!    Carl de Boor
!
!  Reference:
!
!    Carl de Boor,
!    A Practical Guide to Splines,
!    Springer, 2001,
!    ISBN: 0387953663,
!    LC: QA1.A647.v27.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) XKNOT(NKNOT), the sequence of data points.  
!    XKNOT must be strictly increasing.
!
!    Input, real ( kind = 8 ) YKNOT(NKNOT), the corresponding sequence of
!    function values.
!
!    Input, integer ( kind = 4 ) NKNOT, the number of data points.  
!    NKNOT must be at least 4.
!
!    Input, real ( kind = 8 ) GAMMA, indicates whether additional flexibility
!    is desired.
!    GAMMA = 0.0, no additional knots;
!    GAMMA in (0.0,3.0), under certain conditions on the given data at
!    points I-1, I, I+1, and I+2, a knot is added in the I-th interval, 
!    for I = 2,...,NKNOT-2.  See description of method.  The interpolant 
!    gets rounded with increasing gamma.  A value of 2.5 for GAMMA is typical.
!    GAMMA in (3.0,6.0), same, except that knots might also be added in
!    intervals in which an inflection point would be permitted.  A value 
!    of 5.5 for GAMMA is typical.
!
!    Output, real ( kind = 8 ) BREAK(L), real ( kind = 8 ) COEF(K,L), 
!    integer ( kind = 4 ) L, integer K, give the piecewise polynomial 
!    representation of the interpolant.  Specifically, 
!    for BREAK(i) <= X <= BREAK(I+1), the interpolant has the form:
!      F(X) = COEF(1,I) +  DX    * ( 
!             COEF(2,I) + (DX/2) * (
!             COEF(3,I) + (DX/3) *
!             COEF(4,I) ) )
!    with  DX = X - BREAK(I) for I = 1,..., L.
!
!    Output, integer ( kind = 4 ) IER, error flag.
!       modified by Gary Mamon
!    0: no error.
!    1: NKNOT < 4.
!    2: unordered XKNOT(I)
!
!    Output, real ( kind = 8 ) WK(NKNOT,6).  The individual columns of this
!    array contain the following quantities mentioned in the write up
!    and below.
!    WK(.,1) = DXKNOT = XKNOT(.+1)-XKNOT;
!    WK(.,2) = DIAG = diagonal in linear system;
!    WK(.,3) = U = upper diagonal in linear system;
!    WK(.,4) = R = right hand side for linear system (initially)
!           = FSECND = solution of linear system, namely the second
!             derivatives of interpolant at XKNOT;
!    WK(.,5) = Z = indicator of additional knots;
!    WK(.,6) = 1/HSECND(1,X) with X = Z or 1-Z.
!
  implicit none

  integer ( kind = 4 ) nknot

  real ( kind = 8 ) alph
  real ( kind = 8 ) alpha
  real ( kind = 8 ) break(*)
  real ( kind = 8 ) c
  real ( kind = 8 ) coef(4,*)
  real ( kind = 8 ) d
  real ( kind = 8 ) del
  real ( kind = 8 ) denom
  real ( kind = 8 ) divdif
  real ( kind = 8 ) entry
  real ( kind = 8 ) entry3
  real ( kind = 8 ) factor
  real ( kind = 8 ) factr2
  real ( kind = 8 ) gam
  real ( kind = 8 ) gamma
  real ( kind = 8 ) yknot(nknot)
  integer ( kind = 4 ) i
  integer ( kind = 4 ) ier
  integer ( kind = 4 ) k
  integer ( kind = 4 ) l
  integer ( kind = 4 ) method
  real ( kind = 8 ) onemg3
  real ( kind = 8 ) onemzt
  real ( kind = 8 ) ratio
  real ( kind = 8 ) wk(nknot,6)
  real ( kind = 8 ) sixth
  real ( kind = 8 ) xknot(nknot)
  real ( kind = 8 ) temp
  real ( kind = 8 ) x
  real ( kind = 8 ) z
  real ( kind = 8 ) zeta
  real ( kind = 8 ) zt2

  alph(x) = min ( 1.0D+00, onemg3 / x )
!
!  There must be at least 4 interpolation points.
!
  if ( nknot < 4 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'TAUTSP - Fatal error!'
    write ( *, '(a)' ) '  Input NKNOT must be at least 4.'
    write ( *, '(a,i8)' ) '  NKNOT = ', nknot
    ier = 1
    stop 1
  end if
!
!  Construct delta XKNOT and first and second (divided) differences of data.
!
  do i = 1, nknot - 1

    wk(i,1) = xknot(i+1) - xknot(i)
    
    if ( wk(i,1) <= 0.0D+00 ) then
      write ( *, '(a,i3,a,2e15.6,a)' ) &
        '  Point ', i, ' and the next ', xknot(i), xknot(i+1), ' are disordered.'
      ier = 2
      return
    end if
    
    wk(i+1,4) = ( yknot(i+1) - yknot(i) ) / wk(i,1)

  end do
   
  do i = 2, nknot - 1
    wk(i,4) = wk(i+1,4) - wk(i,4)
  end do
!
!  Construct system of equations for second derivatives at XKNOT.
!
!  At each interior data point, there is one continuity equation.
!  At the first and last interior data point there is an additional 
!  equation for a total of NKNOT equations in NKNOT unknowns.
!
  i = 2
  wk(2,2) = wk(1,1) / 3.0D+00
  sixth = 1.0D+00 / 6.0D+00
  method = 2
  gam = gamma

  if ( gam <= 0.0D+00 ) then
    method = 1
  end if

  if ( 3.0D+00 < gam ) then
    method = 3
    gam = gam - 3.0D+00
  end if

  onemg3 = 1.0D+00 - gam / 3.0D+00
!
!  Loop over I.
!
   70 continue
!
!  Construct Z(I) and ZETA(I).
!
  z = 0.5D+00

  if ( method == 1 ) then
    go to 100
  end if

  if ( method == 3 ) then
    go to 90
  end if

  if ( wk(i,4) * wk(i+1,4) < 0.0D+00 ) then
    go to 100
  end if

   90 continue

  temp = abs ( wk(i+1,4) )
  denom = abs ( wk(i,4) ) + temp
  
  if ( denom /= 0.0D+00 ) then
    z = temp / denom
    if ( abs ( z - 0.5D+00 ) <= sixth ) then
      z = 0.5D+00
    end if
  end if
  
  100 continue

  wk(i,5) = z
!
!  Set up part of the I-th equation which depends on the I-th interval.
!
  if ( z < 0.5D+00 ) then

    zeta = gam * z
    onemzt = 1.0D+00 - zeta
    zt2 = zeta**2
    alpha = alph(onemzt)
    factor = zeta / ( alpha * ( zt2 - 1.0D+00 ) + 1.0D+00 )
    wk(i,6) = zeta * factor / 6.0D+00
    wk(i,2) = wk(i,2) + wk(i,1) &
      * ( ( 1.0D+00 - alpha * onemzt ) * factor / 2.0D+00 - wk(i,6) )
!
!  If Z = 0 and the previous Z = 1, then D(I) = 0.  
!  Since then also U(I-1) = L(I+1) = 0, its value does not matter.  
!  Reset D(I) = 1 to insure nonzero pivot in elimination.
!
    if ( wk(i,2) <= 0.0D+00 ) then
      wk(i,2) = 1.0D+00
    end if

    wk(i,3) = wk(i,1) / 6.0D+00

  else if ( z == 0.5D+00 ) then

    wk(i,2) = wk(i,2) + wk(i,1) / 3.0D+00
    wk(i,3) = wk(i,1) / 6.0D+00

  else if ( 0.5D+00 < z ) then

    onemzt = gam * ( 1.0D+00 - z )
    zeta = 1.0D+00 - onemzt
    alpha = alph(zeta)
    factor = onemzt / ( 1.0D+00 - alpha * zeta * ( 1.0D+00 + onemzt ) )
    wk(i,6) = onemzt * factor / 6.0D+00
    wk(i,2) = wk(i,2) + wk(i,1) / 3.0D+00
    wk(i,3) = wk(i,6) * wk(i,1)

  end if

  if ( 2 < i ) then
    go to 190
  end if

  wk(1,5) = 0.5D+00
!
!  The first two equations enforce continuity of the first and of
!  the third derivative across XKNOT(2).
!
  wk(1,2) = wk(1,1) / 6.0D+00
  wk(1,3) = wk(2,2)
  entry3 = wk(2,3)

  if ( z < 0.5D+00 ) then

    factr2 = zeta * ( alpha * ( zt2 - 1.0D+00 ) + 1.0D+00 ) &
      / ( alpha * ( zeta * zt2 - 1.0D+00 ) + 1.0D+00 )

    ratio = factr2 * wk(2,1) / wk(1,2)
    wk(2,2) = factr2 * wk(2,1) + wk(1,1)
    wk(2,3) = - factr2 * wk(1,1)
  
  else if ( z == 0.5D+00 ) then

    ratio = wk(2,1) / wk(1,2)
    wk(2,2) = wk(2,1) + wk(1,1)
    wk(2,3) = - wk(1,1)
  
  else if ( 0.5D+00 < z ) then

    ratio = wk(2,1) / wk(1,2)
    wk(2,2) = wk(2,1) + wk(1,1)
    wk(2,3) = - wk(1,1) * 6.0D+00 * alpha * wk(2,6)

  end if
!
!  At this point, the first two equations read:
!              DIAG(1)*X1+U(1)*X2 + ENTRY3*X3 = R(2)
!       -RATIO*DIAG(1)*X1+DIAG(2)*X2 + U(2)*X3 = 0.0
!  Eliminate first unknown from second equation.
!
  wk(2,2) = ratio * wk(1,3) + wk(2,2)
  wk(2,3) = ratio * entry3 + wk(2,3)
  wk(1,4) = wk(2,4)
  wk(2,4) = ratio * wk(1,4)

  go to 200
  
  190 continue
!
!  The I-th equation enforces continuity of the first derivative
!  across XKNOT(I).  It now reads:
!    - RATIO * DIAG(I-1) * X(I-1) + DIAG(I) * X(I) + U(I) * X(I+1) = R(I).
!  Eliminate (I-1)st unknown from this equation
!
  wk(i,2) = ratio * wk(i-1,3) + wk(i,2)
  wk(i,4) = ratio * wk(i-1,4) + wk(i,4)
!
!  Set up the part of the next equation which depends on the I-th interval.
!
  200 continue

  if ( z < 0.5D+00 ) then

    ratio = - wk(i,6) * wk(i,1) / wk(i,2)
    wk(i+1,2) = wk(i,1) / 3.0D+00

  else if ( z == 0.5D+00 ) then

    ratio = - ( wk(i,1) / 6.0D+00 ) / wk(i,2)
    wk(i+1,2) = wk(i,1) / 3.0D+00

  else if ( 0.5D+00 < z ) then

    ratio = - ( wk(i,1) / 6.0D+00 ) / wk(i,2)
    wk(i+1,2) = wk(i,1) &
      * ( ( 1.0D+00 - zeta * alpha ) * factor / 2.0D+00 - wk(i,6) )

  end if
!
!  End of I loop.
!
  i = i + 1

  if ( i < nknot - 1 ) then
    go to 70
  end if

  wk(i,5) = 0.5D+00
!
!  The last two equations enforce continuity of third derivative and
!  of first derivative across XKNOT(NKNOT-1).
!
  entry = ratio * wk(i-1,3) + wk(i,2) + wk(i,1) / 3.0D+00
  wk(i+1,2) = wk(i,1) / 6.0D+00
  wk(i+1,4) = ratio * wk(i-1,4) + wk(i,4)

  if ( z < 0.5D+00 ) then

    ratio = wk(i,1) * 6.0D+00 * wk(i-1,6) * alpha / wk(i-1,2)
    wk(i,2) = ratio * wk(i-1,3) + wk(i,1) + wk(i-1,1)
    wk(i,3) = - wk(i-1,1)
  
  else if ( z == 0.5D+00 ) then

    ratio = wk(i,1) / wk(i-1,2)
    wk(i,2) = ratio * wk(i-1,3) + wk(i,1) + wk(i-1,1)
    wk(i,3) = - wk(i-1,1)
  
  else if ( 0.5D+00 < z ) then

    factr2 = onemzt * ( alpha * ( onemzt**2 - 1.0D+00 ) + 1.0D+00 ) &
      / ( alpha * ( onemzt**3 - 1.0D+00 ) + 1.0D+00 )

    ratio = factr2 * wk(i,1) / wk(i-1,2)
    wk(i,2) = ratio * wk(i-1,3) + factr2 * wk(i-1,1) + wk(i,1)
    wk(i,3) = - factr2 * wk(i-1,1)

  end if
!
!  At this point, the last two equations read:
!           DIAG(I)*XI+     U(I)*XI+1 = R(I)
!    -RATIO*DIAG(I)*XI+DIAG(I+1)*XI+1 = R(I+1)
!
!  Eliminate XI from the last equation.
!
  wk(i,4) = ratio * wk(i-1,4)
  ratio = - entry / wk(i,2)
  wk(i+1,2) = ratio * wk(i,3) + wk(i+1,2)
  wk(i+1,4) = ratio * wk(i,4) + wk(i+1,4)
!
!  Back substitution.
!
  wk(nknot,4) = wk(nknot,4) / wk(nknot,2)

  do while ( 1 < i )

    wk(i,4) = ( wk(i,4) - wk(i,3) * wk(i+1,4) ) / wk(i,2)
    i = i - 1

  end do

  wk(1,4) = ( wk(1,4) - wk(1,3) * wk(2,4) - entry3 * wk(3,4) ) / wk(1,2)
!
!  Construct polynomial pieces. 
!
  break(1) = xknot(1)
  l = 1

  do i = 1, nknot - 1

    coef(1,l) = yknot(i)
    coef(3,l) = wk(i,4)
    divdif = ( yknot(i+1) - yknot(i) ) / wk(i,1)
    z = wk(i,5)

    if ( z == 0.0D+00 ) then

      coef(2,l) = divdif
      coef(3,l) = 0D+00
      coef(4,l) = 0.0D+00

    else if ( z < 0.5D+00 ) then

      zeta = gam * z
      onemzt = 1.0D+00 - zeta
      c = wk(i+1,4) / 6.0D+00
      d = wk(i,4) * wk(i,6)
      l = l + 1
      del = zeta * wk(i,1)
      break(l) = xknot(i) + del
      zt2 = zeta**2
      alpha = alph(onemzt)
      factor = onemzt**2 * alpha
      coef(1,l) = yknot(i) + divdif * del &
        + wk(i,1)**2 * ( d * onemzt * ( factor - 1.0D+00 ) &
        + c * zeta * ( zt2 - 1.0D+00 ) )
      coef(2,l) = divdif + wk(i,1) * ( d * ( 1.0D+00 - 3.0D+00 * factor ) &
        + c * ( 3.0D+00 * zt2 - 1.0D+00 ) )
      coef(3,l) = 6.0D+00 * ( d * alpha * onemzt + c * zeta )
      coef(4,l) = 6.0D+00 * ( c - d * alpha ) / wk(i,1)
      coef(4,l-1) = coef(4,l) &
        - 6.0D+00 * d * ( 1.0D+00 - alpha ) / ( del * zt2 )
      coef(2,l-1) = coef(2,l) - del * ( coef(3,l) &
        - ( del / 2.0D+00 ) * coef(4,l-1))
 
    else if ( z == 0.5D+00 ) then

      coef(2,l) = divdif &
        - wk(i,1) * ( 2.0D+00 * wk(i,4) + wk(i+1,4) ) / 6.0D+00
      coef(4,l) = ( wk(i+1,4) - wk(i,4) ) / wk(i,1)

    else if ( 0.5D+00 <= z ) then

      onemzt = gam * ( 1.0D+00 - z )

      if ( onemzt == 0.0D+00 ) then

        coef(2,l) = divdif
        coef(3,l) = 0D+00
        coef(4,l) = 0.0D+00

      else

        zeta = 1.0D+00 - onemzt
        alpha = alph(zeta)
        c = wk(i+1,4) * wk(i,6)
        d = wk(i,4) / 6.0D+00
        del = zeta * wk(i,1)
        break(l+1) = xknot(i) + del
        coef(2,l) = divdif - wk(i,1) * ( 2.0D+00 * d + c )
        coef(4,l) = 6.0D+00 * ( c * alpha - d ) / wk(i,1)
        l = l + 1
        coef(4,l) = coef(4,l-1) + 6.0D+00 * ( 1.0D+00 - alpha ) * c &
          / ( wk(i,1) * onemzt**3 )
        coef(3,l) = coef(3,l-1) + del * coef(4,l-1)
        coef(2,l) = coef(2,l-1) + del * ( coef(3,l-1) &
          + ( del / 2.0D+00 ) * coef(4,l-1) )
        coef(1,l) = coef(1,l-1) + del * ( coef(2,l-1) &
          + ( del / 2.0D+00 ) * ( coef(3,l-1) &
          + ( del / 3.0D+00 ) * coef(4,l-1) ) )

      end if

    end if

    l = l + 1
    break(l) = xknot(i+1)
    
  end do
  
  l = l - 1
  k = 4
  ier = 0

  return
end

function ppvalu ( break, coef, l, k, x, jderiv )

!*****************************************************************************80
!
!! PPVALU evaluates a piecewise polynomial function or its derivative.
!
!  Discussion:
!
!    PPVALU calculates the value at X of the JDERIV-th derivative of
!    the piecewise polynomial function F from its piecewise
!    polynomial representation.
!
!    The interval index I, appropriate for X, is found through a
!    call to INTERV.  The formula for the JDERIV-th derivative
!    of F is then evaluated by nested multiplication.
!
!    The J-th derivative of F is given by:
!      (d^J) F(X) = 
!        COEF(J+1,I) + H * (
!        COEF(J+2,I) + H * (
!        ...
!        COEF(K-1,I) + H * (
!        COEF(K,  I) / (K-J-1) ) / (K-J-2) ... ) / 2 ) / 1
!    with
!      H = X - BREAK(I)
!    and
!      I = max ( 1, max ( J, BREAK(J) <= X, 1 <= J <= L ) ).
!
!  Modified:
!
!    16 February 2007
!
!  Author:
!
!    Carl de Boor
!
!  Reference:
!
!    Carl de Boor,
!    A Practical Guide to Splines,
!    Springer, 2001,
!    ISBN: 0387953663,
!    LC: QA1.A647.v27.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) BREAK(L+1), real COEF(*), integer L, the
!    piecewise polynomial representation of the function F to be evaluated.
!
!    Input, integer ( kind = 4 ) K, the order of the polynomial pieces that 
!    make up the function F.  The usual value for K is 4, signifying a 
!    piecewise cubic polynomial.
!
!    Input, real ( kind = 8 ) X, the point at which to evaluate F or
!    of its derivatives.
!
!    Input, integer ( kind = 4 ) JDERIV, the order of the derivative to be
!    evaluated.  If JDERIV is 0, then F itself is evaluated,
!    which is actually the most common case.  It is assumed
!    that JDERIV is zero or positive.
!
!    Output, real ( kind = 8 ) PPVALU, the value of the JDERIV-th
!    derivative of F at X.
!
  implicit none

  integer ( kind = 4 ) k
  integer ( kind = 4 ) l

  real ( kind = 8 ) break(l+1)
  real ( kind = 8 ) coef(k,l)
  real ( kind = 8 ) fmmjdr
  real ( kind = 8 ) h
  integer ( kind = 4 ) i
  integer ( kind = 4 ) jderiv
  integer ( kind = 4 ) m
  integer ( kind = 4 ) ndummy
  real ( kind = 8 ) ppvalu
  real ( kind = 8 ) value
  real ( kind = 8 ) x

  value = 0.0D+00

  fmmjdr = k - jderiv
!
!  Derivatives of order K or higher are identically zero.
!
  if ( k <= jderiv ) then
    return
  end if
!
!  Find the index I of the largest breakpoint to the left of X.
!
  call interv ( break, l+1, x, i, ndummy )
!
!  Evaluate the JDERIV-th derivative of the I-th polynomial piece at X.
!
  h = x - break(i)
  m = k
 
  do

    value = ( value / fmmjdr ) * h + coef(m,i)
    m = m - 1
    fmmjdr = fmmjdr - 1.0D+00

    if ( fmmjdr <= 0.0D+00 ) then
      exit
    end if

  end do

  ppvalu = value
 
  return
end

subroutine interv ( xt, lxt, x, left, mflag )

!*****************************************************************************80
!
!! INTERV brackets a real value in an ascending vector of values.
!
!  Discussion:
!
!    The XT array is a set of increasing values.  The goal of the routine
!    is to determine the largest index I so that 
!
!      XT(I) < XT(LXT)  and  XT(I) <= X.
!
!    The routine is designed to be efficient in the common situation
!    that it is called repeatedly, with X taken from an increasing
!    or decreasing sequence.
!
!    This will happen when a piecewise polynomial is to be graphed.
!    The first guess for LEFT is therefore taken to be the value
!    returned at the previous call and stored in the local variable ILO.
!
!    A first check ascertains that ILO < LXT.  This is necessary
!    since the present call may have nothing to do with the previous
!    call.  Then, if 
!      XT(ILO) <= X < XT(ILO+1), 
!    we set LEFT = ILO and are done after just three comparisons.
!
!    Otherwise, we repeatedly double the difference ISTEP = IHI - ILO
!    while also moving ILO and IHI in the direction of X, until
!      XT(ILO) <= X < XT(IHI)
!    after which we use bisection to get, in addition, ILO + 1 = IHI.
!    The value LEFT = ILO is then returned.
!
!    Thanks to Daniel Gloger for pointing out an important modification
!    to the routine, so that the piecewise polynomial in B-form is
!    left-continuous at the right endpoint of the basic interval,
!    17 April 2014.
!
!  Modified:
!
!    17 April 2014
!
!  Author:
!
!    Carl de Boor
!
!  Reference:
!
!    Carl de Boor,
!    A Practical Guide to Splines,
!    Springer, 2001,
!    ISBN: 0387953663,
!    LC: QA1.A647.v27.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) XT(LXT), a nondecreasing sequence of values.
!
!    Input, integer ( kind = 4 ) LXT, the dimension of XT.
!
!    Input, real ( kind = 8 ) X, the point whose location with 
!    respect to the sequence XT is to be determined.
!
!    Output, integer ( kind = 4 ) LEFT, the index of the bracketing value:
!      1     if             X  <  XT(1)
!      I     if   XT(I)  <= X  < XT(I+1)
!      I     if   XT(I)  <  X == XT(I+1) == XT(LXT)
!
!    Output, integer ( kind = 4 ) MFLAG, indicates whether X lies within the
!    range of the data.
!    -1:            X  <  XT(1)
!     0: XT(I)   <= X  < XT(I+1)
!    +1: XT(LXT) <  X
!
  implicit none

  integer ( kind = 4 ) lxt

  integer ( kind = 4 ) left
  integer ( kind = 4 ) mflag
  integer ( kind = 4 ) ihi
  integer ( kind = 4 ), save :: ilo = 1
  integer ( kind = 4 ) istep
  integer ( kind = 4 ) middle
  real ( kind = 8 ) x
  real ( kind = 8 ) xt(lxt)

  ihi = ilo + 1

  if ( lxt <= ihi ) then

    if ( xt(lxt) <= x ) then
      go to 110
    end if

    if ( lxt <= 1 ) then
      mflag = -1
      left = 1
      return
    end if

    ilo = lxt - 1
    ihi = lxt

  end if

  if ( xt(ihi) <= x ) then
    go to 20
  end if

  if ( xt(ilo) <= x ) then
    mflag = 0
    left = ilo
    return
  end if
!
!  Now X < XT(ILO).  Decrease ILO to capture X.
!
  istep = 1

10 continue

  ihi = ilo
  ilo = ihi - istep

  if ( 1 < ilo ) then
    if ( xt(ilo) <= x ) then
      go to 50
    end if
    istep = istep * 2
    go to 10
  end if

  ilo = 1

  if ( x < xt(1) ) then
    mflag = -1
    left = 1
    return
  end if

  go to 50
!
!  Now XT(IHI) <= X.  Increase IHI to capture X.
!
20 continue

  istep = 1

30 continue

  ilo = ihi
  ihi = ilo + istep

  if ( ihi < lxt ) then

    if ( x < xt(ihi) ) then
      go to 50
    end if

    istep = istep * 2
    go to 30

  end if

  if ( xt(lxt) <= x ) then
    go to 110
  end if
!
!  Now XT(ILO) < = X < XT(IHI).  Narrow the interval.
!
  ihi = lxt

50 continue

  do

    middle = ( ilo + ihi ) / 2

    if ( middle == ilo ) then
      mflag = 0
      left = ilo
      return
    end if
!
!  It is assumed that MIDDLE = ILO in case IHI = ILO+1.
!
    if ( xt(middle) <= x ) then
      ilo = middle
    else
      ihi = middle
    end if

  end do
!
!  Set output and return.
!
110 continue

  mflag = 1

  if ( x == xt(lxt) ) then
    mflag = 0
  end if

  do left = lxt - 1, 1, -1
    if ( xt(left) < xt(lxt) ) then
      return
    end if
  end do

  return
end
