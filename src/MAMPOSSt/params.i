      integer MAXOBJ, MAXINTERP, MAXCOMP
      real*8 SMALL, HUGE, TINY
      real*8 H100, GNewton, MINLNP
      parameter (SMALL=1.d-8, HUGE=1.d30, TINY=1.d-50)
      parameter (H100 = 1.d-3, GNewton = 43.001d0) ! H(z) and G in astrophysical units
		! H in 100km/s / kpc
		! G in kpc (100 km/s)^2 / (10^11 M_Sun)
      parameter (MINLNP=-1.d6)                  ! min (ln p_i) to avoid -infinity
      parameter (MAXOBJ=20000, MAXINTERP=31, MAXCOMP=10)
c
c
c
