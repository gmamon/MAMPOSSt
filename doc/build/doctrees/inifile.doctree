���U      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _initialization_file:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��initialization-file�u�tagname�h	�line�K�parent�hhh�source��./Users/gam/GIT_MAMPOSST/doc/source/inifile.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Example of initialization file�h]�h �Text����Example of initialization file�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �literal_block���)��}�(hX%  # CosmoMC parameters for MAMPOSSt

# root name for files produced
file_root = /Users/gam/COSMOMC/chains/SDSS/test

# data file
data_directory = ..
data_file = Yang2_SF_Passive.dat_mamposst
header_lines = 2

# phase space parameters
#
# v3dmodel is Gaussian (only possibility for now)
# rmax is maximum radius (in kpc) for line-of-sight integrations
v3dmodel = Gaussian
rmax = 40000

# cosmology parameters (flat cosmology is assumed)
#
# z: redshift
# Omegam: z=0 density parameter
# h: dimensionless z=0 Hubble constant (in 100 km/s/Mpc)
# Delta: over-density of the virial radius relative to the critical density of the Universe at observed redshift
# mu0: distance modulus of object, if known (e.g. galaxy in Virgo cluster), else -1
# a0lclM and a1lclM are coeffcients of log concentration = a0lclM + a1lclM * log(M_vir/M_sun), else -1 for both
z = 0.0
Omegam = 0.3
h = 0.7
Delta = 200
mu0 = -1
a0lclM = -1
a1lclM = -1

# MAMPOSSt dark matter model and flags
#
# darkmodel: dark matter model; choices are (g prefix for general inner slope) Einasto, Hernquist, gHernquist, mHubble (King 62), isothermal, Jaffe, Kazantzidis, gKazantzidis, NFW, gNFW, cNFW (cored), Plummer, gPlummer, PrugnielSimien
# darknormflag is meaning of norm parameter: -1 (log r_vir/kpc), 0 (log M_vir/M_Sun), > 0 (r_fid^dark, where norm = log M(r_fid^dark=darknormflag/M_odot)
# darkscaleflag is flag for darkscale: 1 (log scale radius) 2 (log concentration = log r_vir/r_scale)
# darktotflag is flag for norm: 1 (dark only) or 2 (total = dark + tracers)
darkmodel = NFW
darknormflag = -1
darkscaleflag = 2
darktotflag = 1

# other MAMPOSSt flags
#
# anisflag defines velocity anisotropy: 0 (log sig_r/sig_theta), 1 (beta=1-sig_th^2/sig_r^2), 2 (beta_sym = (sig_r^2-sig_th^2)/(sig_r^2+sig_th^2))
# MfLflag: mass follows light
# TLMflag: tied light - mass
# TALflag: tied anisotropy light (anisotropy radius = scale radius of tracer)
# splitpflag: 0 (analysis in projected phase space), 1 (a_tracer is determined separately)
# ilopflag is interloper flag: 0 (halo only), 1 (mixed), 2 (halo and interlopers separately)
# weightflag
# distflag is how to handle redshift-independent distance moduli (mu): 0 (ignore), 1 (weight with Gaussian  mu), 2 (weight with density * Gaussian(mu))
anisflag = 2
MfLflag = 0
TLMflag = 0
TALflag = 0
splitpflag = 0
ilopflag = 1
weightflag = 0
distflag = 0

# other MAMPOSSt parameters
#
# ninterp: number n among n*n cells in R and v_LOS of projected phase space
# debug: 0 (minimal), 1 (more), etc.
ninterp = 0
debug = 0

# components: names of components
components = Passive SF

# tracer fixed parameters (each one multiplied by the number of the components)
#
# tracermodel: choose among Hernquist, mHubble (King 62), Jaffe, NFW, Plummer, PrugnielSimien
# meanltracerradius: estimated log r_tracer/kpc
# sigltracerradius: uncertainty on log r_tracer/kpc
# rfid_tracer: fiducial tracer radius (kpc)
# anismodel: model for the velocity anisotropy (isotropic, cst, OM for Osipkov-Merritt, Tiret)
# Rminallow: minimum projected radius (kpc) allowed for the dynamical analysis
# Rmaxallow: maximum projected radius (kpc) allowed for the dynamical analysis (0 for maximum data projected radius)
# avzmaxallow: maximum absolute value of line-of-sight velocity (km/s) allowed for the dynamical analysis (0 for maximum data absolute line-of-sight velocity)
# wtofR: name of file with weights versus projected radius (kpc), "none" for no file
# CofR: name of file with spectral completeness versus projected radius (kpc), "none" for no file
tracermodel_Passive = NFW
tracermodel_SF = NFW
meanltracerradius_Passive = -1
meanltracerradius_SF = -1
sigltracerradius_Passive = -1
sigltracerradius_SF = -1
rfid_tracer_Passive = 1
rfid_tracer_SF = 1
anismodel_Passive = cst
anismodel_SF = cst
Rminallow_Passive = 50
Rminallow_SF = 50
Rmaxallow_Passive = 2150
Rmaxallow_SF = 2150
avzmaxallow_Passive = 0
avzmaxallow_SF = 0
wtofR_Passive = none
wtofR_SF = none
CofR_Passive = none
CofR_SF = none

# dark matter ajustable parameters
# parameter start center, min, max, start width, propose width
#
# norm: dark matter normalization (according to darknormflag)
# darkscale: dark matter scale (according to darkscaleflag)
# darkpar2: extra dark matter parameter (e.g. inner slope or Einasto index)
param[norm] = 3.25 3 3.5 0.1 0.05
param[darkscale] = 0.5 0 1 0.2 0.1
param[darkpar2] = 1 1 1 0 0

# global tracer parameter: log (total tracer mass / M_Sun) (summed over components)
# parameter start center, min, max, start width, propose width
#
param[ltracermasstot] = -99 -99 -99 0 0

# tracer density ajustable parameters
# parameter start center, min, max, start width, propose width
#
# ltracerradius: log (tracer radius / kpc)
# ltracermass: log (tracer mass / M_Sun)
# tracerpar2: extra tracer parameter (e.g. PrugnielSimien index)
# fractracer: fraction of total tracer mass in component (all 0 if ltracermass is used)
# lanis0: log (anisotropy at r=0) (see anisflag)
# lanisinf: log (anisotropy at r->infinty) (see anisflag)
# lanisradius: log (anisotropy radius / kpc)
param[ltracerradius_Passive] = 2.95 2.5 3.4 0.18 0.09
param[ltracerradius_SF] = 3.45 3 3.9 0.18 0.09
param[ltracermass_Passive] = -99 -99 -99 0 0
param[ltracermass_SF] = -99 -99 -99 0 0
param[tracerpar2_Passive] = 0 0 0 0 0
param[tracerpar2_SF] = 0 0 0 0 0
param[fractracer_Passive] = 0 0 0 0 0
param[fractracer_SF] = 0 0 0 0 0
param[lanis0_Passive] = 0 0 0 0 0
param[lanis0_SF] = 0 0 0 0 0
param[lanisinf_Passive] = 0 0 0 0 0
param[lanisinf_SF] = 0 0 0 0 0
param[lanisradius_Passive] = 0 0 0 0 0
param[lanisradius_SF] = 0 0 0 0 0

# other ajustable parameters
#
# lbhmass: log(black hole mass / M_sun)
# lBilop: log background surface density (kpc^-2)
param[lbhmass] = -99 -99 -99 0 0
param[lBilop] = -99 -99 -99 0 0

# basic CosmoMC parameters (explanations in comments after line)
#
ParamNamesFile = mamposst_SF_Passive.paramnames
samples = 40000
propose_scale = 2.4
#               decrease to increase acceptance ratio and conversely
rand_seed = 1234
#               if blank this is set from system clock
feedback = 0
#               (2=lots,1=chatty,0=less,-1=minimal)
burn_in = 1000
#               0 OK
action = 0
#               MCMC: 0, postprocess: 1, Powell: 2
max_like_radius = 0.005
#               relative accuracy of each parameter in units of the propose width
#               for action=2 [Powell] or or estimate_propose_matrix
sampling_method = 1
estimate_propose_matrix = F
temperature = 1
continue_from =
#               Can re-start from the last line of previous run (.txt file)
oversample_fast = 1
#               Increase to oversample fast parameters,e.g. if space is odd shape
propose_matrix =
#               Covariance matrix can be produced using "getdist" program.
num_threads = 0
#               If zero set automatically

# CosmoMC MPI parameters
# MPI mode multi-chain options (recommended)
# MPI_Converge_Stop is a (variance of chain means)/(mean of variances) parameter that can be used to stop the chains
# Set to a negative number not to use this feature. Does not guarantee good accuracy of confidence limits.
MPI_Converge_Stop = -1
# if MPI_LearnPropose = T, the proposal density is continally updated from the covariance of samples so far (since burn in)
MPI_LearnPropose = T
# Can optionally also check for convergence of confidence limits (after MPI_Converge_Stop  reached)
MPI_Check_Limit_Converge = F
# if MPI_Check_Limit_Converge = T, give tail fraction to check (checks both tails):
MPI_Limit_Converge = 0.025
# And the permitted percentil chain variance in units of the standard deviation (small values v slow):
MPI_Limit_Converge_Err = 0.3
# which parameter's tails to check. If zero, check all parameters:
MPI_Limit_Param = 0
# If have covmat, R to reach before updating proposal density (increase if covmat likely to be poor)
# Only used if not varying new parameters that are fixed in covmat
MPI_Max_R_ProposeUpdate = 2
# As above, but used if varying new parameters that were fixed in covmat
MPI_Max_R_ProposeUpdateNew = 30

# other CosmoMC parameters
# If action = 1
redo_likelihoods = T
redo_theory = F
redo_cls = F
redo_pk = F
redo_skip = 0
redo_outroot =
redo_thin = 1
# If large difference in log likelihoods may need to offset to give sensible weights
# for exp(difference in likelihoods)
redo_likeoffset =  0
# Number of distinct points to sample
# Every accepted point is included
# number of steps between independent samples
# if non-zero all info is dumped to file file_root.data
# if you change this probably have to change output routines in code too
indep_sample = 0

# unused CAMB parameters
# If we are including tensors
compute_tensors = F
#Initial power spectrum amplitude point (Mpc^{-1})
#Note if you change this, may need new .covmat as degeneracy directions change
pivot_k = 0.05
#If using tensors, enforce n_T = -A_T/(8A_s)
inflation_consistency = F
#Set Y_He from BBN constraint; if false set to fixed value of 0.24 by default.
bbn_consistency=T
#Whether the CMB should be lensed (slows a lot unless also computing matter power)
CMB_lensing = T
high_accuracy_default = F
#increase accuracy_level to run CAMB on higher accuracy
#(default is about 0.3%, 0.1% if high_accuracy_default =T)
#accuracy_level can be increased to check stability/higher accuracy, but inefficient
accuracy_level = 1

# unused CosmoMC parameters
cmb_numdatasets = 0
use_CMB = F
use_HST = F
use_mpk = F
use_clusters = F
use_BBN = F
use_Age_Tophat_Prior = F
use_SN = F
use_min_zre = 0�h]�h.X%  # CosmoMC parameters for MAMPOSSt

# root name for files produced
file_root = /Users/gam/COSMOMC/chains/SDSS/test

# data file
data_directory = ..
data_file = Yang2_SF_Passive.dat_mamposst
header_lines = 2

# phase space parameters
#
# v3dmodel is Gaussian (only possibility for now)
# rmax is maximum radius (in kpc) for line-of-sight integrations
v3dmodel = Gaussian
rmax = 40000

# cosmology parameters (flat cosmology is assumed)
#
# z: redshift
# Omegam: z=0 density parameter
# h: dimensionless z=0 Hubble constant (in 100 km/s/Mpc)
# Delta: over-density of the virial radius relative to the critical density of the Universe at observed redshift
# mu0: distance modulus of object, if known (e.g. galaxy in Virgo cluster), else -1
# a0lclM and a1lclM are coeffcients of log concentration = a0lclM + a1lclM * log(M_vir/M_sun), else -1 for both
z = 0.0
Omegam = 0.3
h = 0.7
Delta = 200
mu0 = -1
a0lclM = -1
a1lclM = -1

# MAMPOSSt dark matter model and flags
#
# darkmodel: dark matter model; choices are (g prefix for general inner slope) Einasto, Hernquist, gHernquist, mHubble (King 62), isothermal, Jaffe, Kazantzidis, gKazantzidis, NFW, gNFW, cNFW (cored), Plummer, gPlummer, PrugnielSimien
# darknormflag is meaning of norm parameter: -1 (log r_vir/kpc), 0 (log M_vir/M_Sun), > 0 (r_fid^dark, where norm = log M(r_fid^dark=darknormflag/M_odot)
# darkscaleflag is flag for darkscale: 1 (log scale radius) 2 (log concentration = log r_vir/r_scale)
# darktotflag is flag for norm: 1 (dark only) or 2 (total = dark + tracers)
darkmodel = NFW
darknormflag = -1
darkscaleflag = 2
darktotflag = 1

# other MAMPOSSt flags
#
# anisflag defines velocity anisotropy: 0 (log sig_r/sig_theta), 1 (beta=1-sig_th^2/sig_r^2), 2 (beta_sym = (sig_r^2-sig_th^2)/(sig_r^2+sig_th^2))
# MfLflag: mass follows light
# TLMflag: tied light - mass
# TALflag: tied anisotropy light (anisotropy radius = scale radius of tracer)
# splitpflag: 0 (analysis in projected phase space), 1 (a_tracer is determined separately)
# ilopflag is interloper flag: 0 (halo only), 1 (mixed), 2 (halo and interlopers separately)
# weightflag
# distflag is how to handle redshift-independent distance moduli (mu): 0 (ignore), 1 (weight with Gaussian  mu), 2 (weight with density * Gaussian(mu))
anisflag = 2
MfLflag = 0
TLMflag = 0
TALflag = 0
splitpflag = 0
ilopflag = 1
weightflag = 0
distflag = 0

# other MAMPOSSt parameters
#
# ninterp: number n among n*n cells in R and v_LOS of projected phase space
# debug: 0 (minimal), 1 (more), etc.
ninterp = 0
debug = 0

# components: names of components
components = Passive SF

# tracer fixed parameters (each one multiplied by the number of the components)
#
# tracermodel: choose among Hernquist, mHubble (King 62), Jaffe, NFW, Plummer, PrugnielSimien
# meanltracerradius: estimated log r_tracer/kpc
# sigltracerradius: uncertainty on log r_tracer/kpc
# rfid_tracer: fiducial tracer radius (kpc)
# anismodel: model for the velocity anisotropy (isotropic, cst, OM for Osipkov-Merritt, Tiret)
# Rminallow: minimum projected radius (kpc) allowed for the dynamical analysis
# Rmaxallow: maximum projected radius (kpc) allowed for the dynamical analysis (0 for maximum data projected radius)
# avzmaxallow: maximum absolute value of line-of-sight velocity (km/s) allowed for the dynamical analysis (0 for maximum data absolute line-of-sight velocity)
# wtofR: name of file with weights versus projected radius (kpc), "none" for no file
# CofR: name of file with spectral completeness versus projected radius (kpc), "none" for no file
tracermodel_Passive = NFW
tracermodel_SF = NFW
meanltracerradius_Passive = -1
meanltracerradius_SF = -1
sigltracerradius_Passive = -1
sigltracerradius_SF = -1
rfid_tracer_Passive = 1
rfid_tracer_SF = 1
anismodel_Passive = cst
anismodel_SF = cst
Rminallow_Passive = 50
Rminallow_SF = 50
Rmaxallow_Passive = 2150
Rmaxallow_SF = 2150
avzmaxallow_Passive = 0
avzmaxallow_SF = 0
wtofR_Passive = none
wtofR_SF = none
CofR_Passive = none
CofR_SF = none

# dark matter ajustable parameters
# parameter start center, min, max, start width, propose width
#
# norm: dark matter normalization (according to darknormflag)
# darkscale: dark matter scale (according to darkscaleflag)
# darkpar2: extra dark matter parameter (e.g. inner slope or Einasto index)
param[norm] = 3.25 3 3.5 0.1 0.05
param[darkscale] = 0.5 0 1 0.2 0.1
param[darkpar2] = 1 1 1 0 0

# global tracer parameter: log (total tracer mass / M_Sun) (summed over components)
# parameter start center, min, max, start width, propose width
#
param[ltracermasstot] = -99 -99 -99 0 0

# tracer density ajustable parameters
# parameter start center, min, max, start width, propose width
#
# ltracerradius: log (tracer radius / kpc)
# ltracermass: log (tracer mass / M_Sun)
# tracerpar2: extra tracer parameter (e.g. PrugnielSimien index)
# fractracer: fraction of total tracer mass in component (all 0 if ltracermass is used)
# lanis0: log (anisotropy at r=0) (see anisflag)
# lanisinf: log (anisotropy at r->infinty) (see anisflag)
# lanisradius: log (anisotropy radius / kpc)
param[ltracerradius_Passive] = 2.95 2.5 3.4 0.18 0.09
param[ltracerradius_SF] = 3.45 3 3.9 0.18 0.09
param[ltracermass_Passive] = -99 -99 -99 0 0
param[ltracermass_SF] = -99 -99 -99 0 0
param[tracerpar2_Passive] = 0 0 0 0 0
param[tracerpar2_SF] = 0 0 0 0 0
param[fractracer_Passive] = 0 0 0 0 0
param[fractracer_SF] = 0 0 0 0 0
param[lanis0_Passive] = 0 0 0 0 0
param[lanis0_SF] = 0 0 0 0 0
param[lanisinf_Passive] = 0 0 0 0 0
param[lanisinf_SF] = 0 0 0 0 0
param[lanisradius_Passive] = 0 0 0 0 0
param[lanisradius_SF] = 0 0 0 0 0

# other ajustable parameters
#
# lbhmass: log(black hole mass / M_sun)
# lBilop: log background surface density (kpc^-2)
param[lbhmass] = -99 -99 -99 0 0
param[lBilop] = -99 -99 -99 0 0

# basic CosmoMC parameters (explanations in comments after line)
#
ParamNamesFile = mamposst_SF_Passive.paramnames
samples = 40000
propose_scale = 2.4
#               decrease to increase acceptance ratio and conversely
rand_seed = 1234
#               if blank this is set from system clock
feedback = 0
#               (2=lots,1=chatty,0=less,-1=minimal)
burn_in = 1000
#               0 OK
action = 0
#               MCMC: 0, postprocess: 1, Powell: 2
max_like_radius = 0.005
#               relative accuracy of each parameter in units of the propose width
#               for action=2 [Powell] or or estimate_propose_matrix
sampling_method = 1
estimate_propose_matrix = F
temperature = 1
continue_from =
#               Can re-start from the last line of previous run (.txt file)
oversample_fast = 1
#               Increase to oversample fast parameters,e.g. if space is odd shape
propose_matrix =
#               Covariance matrix can be produced using "getdist" program.
num_threads = 0
#               If zero set automatically

# CosmoMC MPI parameters
# MPI mode multi-chain options (recommended)
# MPI_Converge_Stop is a (variance of chain means)/(mean of variances) parameter that can be used to stop the chains
# Set to a negative number not to use this feature. Does not guarantee good accuracy of confidence limits.
MPI_Converge_Stop = -1
# if MPI_LearnPropose = T, the proposal density is continally updated from the covariance of samples so far (since burn in)
MPI_LearnPropose = T
# Can optionally also check for convergence of confidence limits (after MPI_Converge_Stop  reached)
MPI_Check_Limit_Converge = F
# if MPI_Check_Limit_Converge = T, give tail fraction to check (checks both tails):
MPI_Limit_Converge = 0.025
# And the permitted percentil chain variance in units of the standard deviation (small values v slow):
MPI_Limit_Converge_Err = 0.3
# which parameter's tails to check. If zero, check all parameters:
MPI_Limit_Param = 0
# If have covmat, R to reach before updating proposal density (increase if covmat likely to be poor)
# Only used if not varying new parameters that are fixed in covmat
MPI_Max_R_ProposeUpdate = 2
# As above, but used if varying new parameters that were fixed in covmat
MPI_Max_R_ProposeUpdateNew = 30

# other CosmoMC parameters
# If action = 1
redo_likelihoods = T
redo_theory = F
redo_cls = F
redo_pk = F
redo_skip = 0
redo_outroot =
redo_thin = 1
# If large difference in log likelihoods may need to offset to give sensible weights
# for exp(difference in likelihoods)
redo_likeoffset =  0
# Number of distinct points to sample
# Every accepted point is included
# number of steps between independent samples
# if non-zero all info is dumped to file file_root.data
# if you change this probably have to change output routines in code too
indep_sample = 0

# unused CAMB parameters
# If we are including tensors
compute_tensors = F
#Initial power spectrum amplitude point (Mpc^{-1})
#Note if you change this, may need new .covmat as degeneracy directions change
pivot_k = 0.05
#If using tensors, enforce n_T = -A_T/(8A_s)
inflation_consistency = F
#Set Y_He from BBN constraint; if false set to fixed value of 0.24 by default.
bbn_consistency=T
#Whether the CMB should be lensed (slows a lot unless also computing matter power)
CMB_lensing = T
high_accuracy_default = F
#increase accuracy_level to run CAMB on higher accuracy
#(default is about 0.3%, 0.1% if high_accuracy_default =T)
#accuracy_level can be increased to check stability/higher accuracy, but inefficient
accuracy_level = 1

# unused CosmoMC parameters
cmb_numdatasets = 0
use_CMB = F
use_HST = F
use_mpk = F
use_clusters = F
use_BBN = F
use_Age_Tophat_Prior = F
use_SN = F
use_min_zre = 0�����}�(hhhh;ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhh9hKhh$hhh h!ubeh}�(h]�(�example-of-initialization-file�heh]�h]�(�example of initialization file��initialization_file�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�hQhs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�h{�error_encoding��US-ASCII��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(hQhhPhMu�	nametypes�}�(hQ�hPNuh}�(hh$hMh$u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h �	paragraph���)��}�(hhh]�h.�9Hyperlink target "initialization-file" is not referenced.�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhh�uba�transformer�N�
decoration�Nhhub.