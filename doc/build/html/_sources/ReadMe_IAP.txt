Running MAMPOSSt at IAP
=======================

Selection of computers
-----------------------
Run script::

	~gam/script/chkfastlinux -8 

to see available 8-core computers (allowing up to 8 chains). For reasons
of etiquette, limit the number of chains to 6 on 8-core machines.

Launching on several computers
------------------------------
Run script::

	~gam/script/launchcosmomcruns [file]

to launch different runs on different IAP machines. This script reads the
file (default: ``./launchcosmomcruns.txt``) of 2-column rows with the IAP
computer in the 1st column and the run name in the 2nd column. This file is
built manually.





