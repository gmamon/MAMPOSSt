This is the git repository for MAMPOSSt.

* Documenatation is in build/html/index.html (to be read with a navigator).

* To install:

  % INSTALL.csh

(where `%' is the UNIX prompt)

* Documentation

  open navigator on doc/build/html/index.html

* To run test program (INSTALL.csh should do this for you, but if you want to
  try again without installing)

  % cd test
  % buildini_mamposst -data_dir . -data_file test.dat -n 2 Passive SF -lrtr 2.5 3.4 2.9 3.8 test

* To plot test results (assuming you have SM)

  % sm
  sm> mosaicMCMC test 10000

(where `sm>' is the SM prompt)

gv (`open' on Mac) test.pdf



