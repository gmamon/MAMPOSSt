#! /bin/tcsh -f

# This is a C-shell installation script to instal properly MAMPOSSt.
# Author: Gary Mamon

set dir_script_default = script
set dir_bin_default = script
set dir_nosave_default = NOSAVE
set dir_SM_default = SM
set plotsuffix = pdf
set rootdir = `pwd`

if ($OSTYPE == darwin) then
	set getcmd = brew
	set getcmdname = Homebrew
else
	set getcmd = (sudo apt)
	set getcmdname = apt
endif

if (`which $getcmd |& grep -v 'Command not found' | grep -v alias` == 0) then
	set getcmd = none
endif
	
# Fortran compiler

set ifort = 0
set gfortran = 0
if (`which ifort |& grep -v 'Command not found' | grep -vc alias` == 1) then
	set ifort = 1
endif
if (`which gfortran |& grep -v 'Command not found' | grep -vc alias` == 1) then
	set gfortran = 1
endif

if ($ifort && $gfortran) then
	echo "Your system has both ifort & gfortran compilers"
	echo -n "Pick your Fortran compiler (i: ifort, g: gfortran, default: i): "
	set ans = $<
	switch ($ans)
	case g:
		setenv FORTRAN gfortran
		breaksw
	case i:
	case "":
		setenv FORTRAN ifort
		breaksw
	default:
		exec echo cannot recognize $ans ...
	endsw
else if ($ifort) then
	setenv FORTRAN ifort
else if ($gfortran) then
	setenv FORTRAN gfortran
else
	exec echo Cannot find gfortran nor ifort for Fortran compilation\!
endif

if (`which mpif90 |& grep -v 'Command not found' | grep -vc alias` == 0) then
	echo -n "You do not have MPI on your system. Do you want to install it here with $getcmdname? (y|n) "
	set ans = $<
	if ($ans == y) then
		if ("$getcmd" != none) then
			$getcmd install mpich
		else
			exec echo "you must first install mpich on your system"
		endif
	endif
endif

# Compile files

rehash

if (! -d ./lib) then
	mkdir ./lib
endif
if (! -d ./lib/lib_$FORTRAN) then
	mkdir ./lib/lib_$FORTRAN
endif
# touch ./lib/lib_$FORTRAN/libutils.a
# echo Compiling code ...
cd ./src
make cleanall
# make "FORTRAN=$FORTRAN"
# echo ""
cd ..

# Directory for script files

echo -n "Enter directory for MAMPOSSt script files (relative to home directory, default: $dir_script_default): "
set dir_script_suffix = $<
if ($dir_script_suffix == "") then
	set dir_script_suffix = $dir_script_default
endif
set dir_script = $HOME/$dir_script_suffix
if (! -d $dir_script) then
	if (-r $dir_script) then
		echo Moving old file $dir_script to ${dir_script}.BAK ...
		mv $dir_script ${dir_script}.BAK
	endif
	echo Creating directory $dir_script ...
	mkdir $dir_script
	echo ""
endif

# Add script directory to path if necessary

set found = 0
foreach dir ($path)
	if ($dir == $dir_script) then
		set found = 1
		break
	endif
end
if ($found == 1) then
	echo $dir_script is confirmed to be in your path
else
	exec echo "$dir_script is not in your path; add it to your path by hand and relaunch $0."
endif

# Copy MAMPOSSt script files to script directory

cd ./script
echo Copying script files to $dir_script directory ... 
cp autoruncosmomc buildini_mamposst getcosmomcruns $dir_script
cd ..
echo ""

# Unsaved directory

echo Setting up the NOSAVE directory ...
echo -n "Enter unsaved directory (relative to home directory) where you want the large output files and MCMC chains to end up (default: $dir_nosave_default is strongly recommended, otherwise you will need to modify the sm macros): "
set dir_nosave_suffix = $<
if ($dir_nosave_suffix == "") then
	set dir_nosave_suffix = $dir_nosave_default
endif
set dir_nosave = $HOME/$dir_nosave_suffix

if (! -d $dir_nosave) then
	if (-r $dir_nosave) then
		echo Moving $dir_nosave to $dir_nosave.BAK ...
		mv $dir_nosave $dir_nosave.BAK
	endif
	echo Creating directories $dir_nosave, $dir_nosave/COSMOMC and $dir_nosave/COSMOMC/chains ...
	mkdir $dir_nosave
	mkdir $dir_nosave/COSMOMC
	mkdir $dir_nosave/COSMOMC/chains
endif
echo ""

# SM set up: check in order for
# 1) sm command
# 2) ~/.sm file
# 3) macro2 entry in ~/.sm file
# 4) macro2 directory
# 5) `default' SM macro file in macro2 directory
# 6) startup SM macro in `default' SM macro file
# 7) m2files list of macros in startup SM macro
# 8) mamposst in list of m2files

# SM file

set have_SM = `which sm |& grep -vc 'Command not found'`

if ($have_SM) then	# sm command is on system
	if (-r ~/.sm) then	# .sm file is in home directory
		if (`awk '$1 == "macro2"' ~/.sm | wc -l` > 0) then	# .sm file has macro2 entry
			set macro2 = `awk '$1 == "macro2" { print $2}' ~/.sm`
			if (-d $macro2) then	# macro2 directory of SM macros exists
				# file of SM macros for MAMPOSSt
				set copy_mamposst = n
				if (-r ${macro2}mamposst) then	# mamposst SM macro file already exists
					echo "There is already a mamposst SM macro file in your $macro2:h macro2 directory"
					echo -n "Do you want to replace it? (y|n) "
					set copy_mamposst = $<
				else
					set copy_mamposst = y
				endif
				if ($copy_mamposst == y) then
					echo copying tools/mamposst.sm to ${macro2}mamposst ...
					cp -f tools/mamposst.sm ${macro2}mamposst
				endif

				# default file
				if (-r ${macro2}default) then	# default SM macro file is present
					set values = `awk 'BEGIN { found_startup2 = 0; end_startup2 = 0; found_plotsuffix = 0; found_m2files = 0; found_mamposst = 0 } /startup2/ { found_startup2 = 1; nr0 = NR } found_startup2 == 1 && end_startup2 == 0 && NR > nr0 && substr($0,1,1) != "\t" { end_startup2 = 1 } found_startup2 == 1 && end_startup2 == 0 && NR > nr0 { if (index($0,"	define plotsuffix") > 0) found_plotsuffix = 1; if (index($0,"	set m2files =") > 0) {found_m2files = 1; if (index($0,"mamposst") > 0) found_mamposst = 1}} END {print found_startup2, found_plotsuffix, found_m2files, found_mamposst}' ${macro2}default`
					set found_startup2 = $values[1]
					set found_plotsuffix = $values[2]
					set found_m2files = $values[3]
					set found_mamposst = $values[4]

					if ($found_startup2 == 0) then
						echo creating startup2
						cat >> ${macro2}default << EOF
startup2	# additional commands on SM startup
	define plotsuffix $plotsuffix
	set m2files = {mamposst}
	foreach lib m2files {
		macro read "\$!macro2"\$lib
	}
EOF
					else
						if ($found_plotsuffix == 0) then
							# insert plotsuffix in startup2 macro
							echo Inserting plotsuffix = $plotsuffix in startup2 macro of ${macro2}default ...
							awk -v plotsuffix=$plotsuffix '/^startup/ {print; print "\tdefine plotsuffix ", plotsuffix; next}1' ${macro2}default > tmp$$
							mv -f tmp$$ ${macro2}default
						endif
						if ($found_m2files == 0) then
							# insert m2files in startup2 macro
							echo Inserting \"set m2files = {mamposst}\" in startup2 macro of ${macro2}default ...
							awk  '/^startup/ {print; print "\tset m2files = {mamposst}"; next}1' ${macro2}default > tmp$$
							mv -f tmp$$ ${macro2}default
						else
							if ($found_mamposst == 0) then
								# insert mamposst among m2files
								echo Inserting \"mamposst\" among macros that are automaticaly loaded by SM ...
								awk '! /set m2files =/ { print } /set m2files =/ { printf("\t"); for(i=1;i<NF-1;i++) printf("%s ", $i); printf("mamposst %s\n", $NF)}' ${macro2}default > tmp$$
								mv -f tmp$$ ${macro2}default
							endif
						endif
					endif
				else # no default file
					echo Copying SM macro file tools/default.sm to ${macro2}default ...
					cp -f tools/default.sm ${macro2}default
				endif
			else # no macro2 directory of macros
				echo Creating $macro2 directory ...
				mkdir $macro2
				echo Copying SM macro file tools/default.sm to ${macro2}default ...
				cp -f tools/default.sm ${macro2}default
			endif
		else # no macro2 entry in .sm file
			echo Inserting macro2 line in $HOME/.sm ...
			echo "macro2	SM" >> ~/.sm
			echo Creating $macro2 directory ...
			mkdir $macro2
			echo Copying SM macro file tools/default.sm to ${macro2}default ...
			cp -f tools/default.sm ${macro2}default
		endif

		# check for TeX_strings 1 in .sm file

		if (`awk '$1 == "TeX_strings"' ~/.sm | wc -l` > 0) then
			if (`awk '$1 == "TeX_strings" && $2 != 1' ~/.sm | wc -l` > 0) then
				awk '$1 != "TeX_strings" { print } $1 == "TeX_strings" { print "TeX_strings	1" }' ~/.sm > tmp$$
				mv -f tmp$$ ~/.sm
			else
				echo "TeX_strings	1" >> ~/.sm
			endif
		endif
	else # no .sm file
		echo You need to create your $HOME/.sm SM initialization file\! See https://www.astro.princeton.edu/~rhl/sm/sm.html
		if (-d /usr/local/lib/sm) then
			set smlibdir = /usr/local/lib/sm
		else
			set ok = 0
			while ($ok == 0)
				echo -n "Enter full directory path of where sm libraries are located on your system (e.g. .../lib/sm): "
				set smlibdir = $<
				# remove trailing slash
				set smlibdir = `echo $smlibdir | sed 's,/$,,'`
			
				if (! -d $smlibdir) then
					echo "Warning: $smlibdir does not exist!"
				else if (! -d $smlibdir/macro) then
					echo "Warning: $smlibdir exists, but does not include $smlibdir/macro subdirectory!"
				else if (! -r $smlibdir/macro/default) then
					echo "Warning: $smlibdir/macro exists, but does not include \'default\' file within it!"
				else
					set ok = 1
					
				endif
			end
		endif
		echo -n "Enter directory (relative to your home directory) where you wish to have your local sm directory: "
		set smdir = $<
		# remove trailing slash
		set smdir = `echo $smdir | sed 's,/$,,'`
		set macro2 = $smdir/macro2
		cat > ~/.sm << EOF
savefile	.smsave
history_file	.smhist
prompt	sm>
history_char	`
graphcap	$smlibdir/graphcap
filecap	$smlibdir/filecap
TeX_strings	1
macro	$smlibdir/macro/
macro2	$smdir/macro2/
table_type	bintable
file_type	unix
EOF			

		echo Creating $macro2 directory ...
		mkdir $macro2
		echo Copying SM macro file tools/default.sm to ${macro2}default ...
		cp -f tools/default.sm ${macro2}default
	endif
else
	echo "You don't have SM on your system, so you will need to write your own plotting macros for the MCMC marginal distributions and covariance plots, which ought to look like the file test/example.pdf"
endif
echo ""

echo "You are ready to try to (compile and) run the MAMPOSSt test case (takes 5 minutes)."
echo -n "Do you want to do this now? (y|n) "
set ans = $<
if ($ans != y) then
	exit
endif

cd test
../script/buildini_mamposst -root_dir $rootdir -data_dir . -data_file test.dat -chaindir TESTS -n 2 Passive SF -lrtr 2.5 3.4 3 3.9 -rmax 40000 test
